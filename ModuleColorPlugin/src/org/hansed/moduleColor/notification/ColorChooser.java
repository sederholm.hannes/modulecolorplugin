package org.hansed.moduleColor.notification;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.ui.EditorNotifications;
import org.hansed.moduleColor.components.ColorStorage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @author HanSed
 */
class ColorChooser {

    /**
     * Create a new color chooser dialog for the user to select a notifications color
     *
     * @param moduleName name of the module whose color is to be changed
     * @param color      the current color of that module
     */
    static void create(String moduleName, Color color) {
        JColorChooser chooser = new JColorChooser(color);

        ActionListener cancelAction = actionEvent -> {
        };

        ActionListener okAction = actionEvent -> {
            Color newColor = chooser.getColor();
            ApplicationManager.getApplication().getService(ColorStorage.class).setColor(moduleName, newColor);
            EditorNotifications.updateAll();
        };

        JDialog dialog = JColorChooser.createDialog(null, "Choose a color", true, chooser, okAction, cancelAction);
        dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        dialog.setVisible(true);
    }
}
